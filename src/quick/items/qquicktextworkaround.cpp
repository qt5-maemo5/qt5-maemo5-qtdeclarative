#include <QtGui/qpainter.h>
#include "qquicktextworkaround.h"

QQuickTextWorkAround::QQuickTextWorkAround(QQuickItem *parent) : QQuickPaintedItem(parent)
{
    setFlag(ItemHasContents, true);
    setWidth(10);
    setHeight(10);
    text_p="";
    font_p=QFont();
    color_p = qRgb(0,0,0);
}

void QQuickTextWorkAround::paint(QPainter *painter)
{
    QPen pen = painter->pen();
    pen.setColor(color_p);
    painter->setPen(pen);
    //painter->setBrush(QColor(color_p));
    painter->setFont(font_p);
    
    QFontMetrics fm(font_p);
    
//     QLabel *lab = new QLabel();
    
//     lab->setText(text_p);
//     lab->setFont(font_p);
//     lab->setMargin(20);
//     lab->setStyleSheet("color:rgba(0, 0, 0, 255); background-color: rgba(0, 0, 0, 0);");
//     lab->setWordWrap(false);
//     lab->resize(lab->sizeHint());
//     setSize(lab->sizeHint());
//     lab->render(painter);
    painter->drawText(QPoint(0,fm.height()),text_p);
    return;
} 


QFont QQuickTextWorkAround::font() const
{
	return font_p;
}

void QQuickTextWorkAround::setFont(const QFont &font)
{ 
    if (font_p == font)
        return;
    font_p = font;

    if (font_p.pointSizeF() != -1) {
        // 0.5pt resolution
        qreal size = qRound(font_p.pointSizeF()*2.0);
        font_p.setPointSizeF(size/2.0);
    }
    
    QFontMetrics fm(font_p);
    setWidth(fm.width(text_p)+20);
    setHeight(fm.height()+10);

    update();
    emit fontChanged(font_p);
}

QString QQuickTextWorkAround::text() const
{
    return text_p; 
}

void QQuickTextWorkAround::setText(const QString &n)
{
	 
    if(text_p == n)
        return;
    
    text_p = n;
    
    QFontMetrics fm(font_p);
    setWidth(fm.width(text_p)+20);
    setHeight(fm.height()+10);
    
    update();
    emit textChanged(text_p);
}


QColor QQuickTextWorkAround::color() const
{
	return QColor(color_p);
}

void QQuickTextWorkAround::setColor(const QColor &color)
{ 
    if (color_p == color.rgba())
        return;
    color_p = color.rgba();

    update();
    emit colorChanged();
}