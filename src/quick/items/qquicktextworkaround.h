#include "qquickpainteditem.h"
#include <QtWidgets/qlabel.h>

class QQuickTextWorkAround : public QQuickPaintedItem {
    Q_OBJECT
    Q_ENUMS(HAlignment)
    Q_ENUMS(VAlignment)
    Q_ENUMS(TextStyle)
    Q_ENUMS(TextFormat)
    Q_ENUMS(TextElideMode)
    Q_ENUMS(WrapMode)
    Q_ENUMS(LineHeightMode)
    Q_ENUMS(FontSizeMode)
    Q_ENUMS(RenderType)
    
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(QFont font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
//     Q_PROPERTY(QColor linkColor READ linkColor WRITE setLinkColor NOTIFY linkColorChanged)
//     Q_PROPERTY(TextStyle style READ style WRITE setStyle NOTIFY styleChanged)
//     Q_PROPERTY(QColor styleColor READ styleColor WRITE setStyleColor NOTIFY styleColorChanged)
//     Q_PROPERTY(HAlignment horizontalAlignment READ hAlign WRITE setHAlign RESET resetHAlign NOTIFY horizontalAlignmentChanged)
//     Q_PROPERTY(HAlignment effectiveHorizontalAlignment READ effectiveHAlign NOTIFY effectiveHorizontalAlignmentChanged)
//     Q_PROPERTY(VAlignment verticalAlignment READ vAlign WRITE setVAlign NOTIFY verticalAlignmentChanged)
//     Q_PROPERTY(WrapMode wrapMode READ wrapMode WRITE setWrapMode NOTIFY wrapModeChanged)
//     Q_PROPERTY(int lineCount READ m_lineCount NOTIFY lineCountChanged)
//     Q_PROPERTY(bool truncated READ truncated NOTIFY truncatedChanged)
//     Q_PROPERTY(int maximumLineCount READ maximumLineCount WRITE setMaximumLineCount NOTIFY maximumLineCountChanged RESET resetMaximumLineCount)
// 
//     Q_PROPERTY(TextFormat textFormat READ textFormat WRITE setTextFormat NOTIFY textFormatChanged)
//     Q_PROPERTY(TextElideMode elide READ elideMode WRITE setElideMode NOTIFY elideModeChanged) //### elideMode?
//     Q_PROPERTY(qreal contentWidth READ contentWidth NOTIFY contentSizeChanged)
//     Q_PROPERTY(qreal contentHeight READ contentHeight NOTIFY contentSizeChanged)
//     Q_PROPERTY(qreal paintedWidth READ contentWidth NOTIFY contentSizeChanged)  // Compatibility
//     Q_PROPERTY(qreal paintedHeight READ contentHeight NOTIFY contentSizeChanged)
//     Q_PROPERTY(qreal lineHeight READ lineHeight WRITE setLineHeight NOTIFY lineHeightChanged)
//     Q_PROPERTY(LineHeightMode lineHeightMode READ lineHeightMode WRITE setLineHeightMode NOTIFY lineHeightModeChanged)
//     Q_PROPERTY(QUrl baseUrl READ baseUrl WRITE setBaseUrl RESET resetBaseUrl NOTIFY baseUrlChanged)
//     Q_PROPERTY(int minimumPixelSize READ minimumPixelSize WRITE setMinimumPixelSize NOTIFY minimumPixelSizeChanged)
//     Q_PROPERTY(int minimumPointSize READ minimumPointSize WRITE setMinimumPointSize NOTIFY minimumPointSizeChanged)
//     Q_PROPERTY(FontSizeMode fontSizeMode READ fontSizeMode WRITE setFontSizeMode NOTIFY fontSizeModeChanged)
//     Q_PROPERTY(RenderType renderType READ renderType WRITE setRenderType NOTIFY renderTypeChanged)
    
public:
    explicit QQuickTextWorkAround(QQuickItem *parent = 0);
    void paint(QPainter *painter);
    
    QString text() const;
    void setText(const QString &);

    QFont font() const;
    void setFont(const QFont &font);
    
    QColor color() const;
    void setColor(const QColor &color);
    
Q_SIGNALS:
    void textChanged(const QString &text);
    void fontChanged(const QFont &font);
    void colorChanged();
    
    
private:
    QFont font_p;
    QString text_p;
    QRgb color_p;
};
